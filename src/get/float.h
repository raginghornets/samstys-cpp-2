#include <iostream>
#include <string>

#ifndef floath
#define floath
namespace get {
  float floatf(std::string prompt) {
    float res = 0;
    std::cout << prompt;
    try { std::cin >> res; } catch (...) {}
    std::cin.get(); 
    return res;
  }
}
#endif