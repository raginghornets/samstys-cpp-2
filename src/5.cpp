#include "get/float.h"

int main(int argc, char* argv[]) {
  float dividend = get::floatf("Dividend: ");
  if (dividend == 0) {
    std::cout << 0;
    return 0;
  }

  float divisor = get::floatf("Divisor: ");
  if (dividend != 0 && divisor == 0) {
    std::cerr << "undefined";
    return 1;
  };

  float result = (dividend / divisor);
  std::cout << result << std::endl;
  return 0;
}