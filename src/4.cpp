#include <iostream>

int main(int argc, char* argv[]) {
  std::cout << "Dividend..." << std::endl;

  float dividend = 1;
  std::cout << "Dividend: ";
  std::cin >> dividend;

  std::cout << "Divisor..." << std::endl;

  float divisor = 1;
  std::cout << "Divisor: ";
  std::cin >> divisor;

  std::cout << "Calculating..." << std::endl;  

  float result = (dividend / divisor);
  std::cout << result << std::endl;

  std::cout << "Calculation done." << std::endl;

  return 0;
}